#!/bin/bash

echo ""
echo "discovery:"
echo ""
echo "# ffmpeg -list_devices 1 -f decklink -i dummy"
echo "# ffmpeg -list_formats 1 -f decklink -i 'DeckLink SDI (1)'"
echo ""
echo "record:"
echo ""
echo "# ffmpeg -y -f decklink -format_code Hi50 -i 'DeckLink SDI (1)' -c:a copy -c:v copy /srv/out/output.avi"
echo "# ffmpeg -y -f decklink -format_code Hi50 -i 'DeckLink SDI (1)' -c:a aac -b:a 128k -vf yadif -c:v libx264 -crf 23 /srv/out/output.mp4"
echo ""
exec docker run --name ffmpeg-test -it --rm --cap-add=SYS_NICE \
       --device=/dev/blackmagic:/dev/blackmagic  \
       -v /usr/lib/blackmagic:/usr/lib/blackmagic \
       -v /usr/lib/libDeckLinkAPI.so:/usr/lib/libDeckLinkAPI.so \
       -v /usr/lib/libDeckLinkPreviewAPI.so:/usr/lib/libDeckLinkPreviewAPI.so \
       -v /tmp/out:/srv/out spreadspace/ffmpeg bash
