ARG CODENAME
FROM registry.gitlab.com/spreadspace/docker/foundation/debian:${CODENAME}

ARG FFMPEG_VERSION
RUN set -x \
    && apt-get update -q \
    && apt-get install -y -q ffmpeg=${FFMPEG_VERSION} python3 python3-ruamel.yaml \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY scripts/ /usr/local/bin/

USER app
