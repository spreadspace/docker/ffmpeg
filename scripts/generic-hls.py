#!/usr/bin/python3 -u

import sys
import os

import ruamel.yaml as yaml
import collections.abc


def config_update(d, u):
    if u == None:
        return d
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = config_update(d.get(k, {}), v)
        elif v is None:
            try:
                del d[k]
            except KeyError:
                pass
        else:
            d[k] = v
    return d


# *******************************************************
# configuration
config = {}

config['quality_levels'] = {}
config['quality_levels']['full'] = {'resolution': '1080p', 'bitrate': 5000, 'scale-to': None}
config['quality_levels']['high'] = {'resolution': '720p', 'bitrate': 3000, 'scale-to': 'hd720'}
config['quality_levels']['medium'] = {'resolution': '480p', 'bitrate': 2000, 'scale-to': 'hd480'}
config['quality_levels']['low'] = {'resolution': '360p', 'bitrate': 1000, 'scale-to': 'nhd'}
config['quality_levels']['mini'] = {'resolution': '240p', 'bitrate': 500, 'scale-to': 'fwqvga'}

config['audio_tracks'] = {}
config['audio_tracks']['all'] = {'input_stream': '0:a', 'bitrate': 512}
config['audio_tracks']['orig'] = {'input_stream': '0:a', 'channels': [0, 1], 'bitrate': 128}
config['audio_tracks']['interpreter'] = {'input_stream': '0:a', 'channels': [2], 'bitrate': 64}
config['audio_tracks']['en'] = {'input_stream': '0:a', 'channels': [4, 5], 'bitrate': 128}
config['audio_tracks']['de'] = {'input_stream': '0:a', 'channels': [6, 7], 'bitrate': 128}

config['x264'] = {}
config['x264']['crf'] = 21
config['x264']['profile'] = 'main'
config['x264']['preset'] = 'medium'
config['x264']['params'] = ['colorprim=bt709', 'transfer=bt709', 'colormatrix=bt709', 'fullrange=off']

config['video_keyframe_distance'] = 2

config['hls'] = {}
config['hls']['segment_time'] = 10
config['hls']['list_size'] = 3
config['hls']['delete_segments'] = True
config['hls']['target'] = '.'


config_filename = os.environ.get('CONFIG_FILENAME')
if config_filename:
    with open(config_filename) as config_file:
        custom_config = yaml.load(config_file, Loader=yaml.SafeLoader)
        config_update(config, custom_config)
        print("successfully loaded config file: '%s'" % (config_filename))

print('configuration:\n')
print(yaml.dump(config, default_flow_style=False, default_style=''))
print('\n')


# *******************************************************
# check command-line arguments, environment variables and config

if len(sys.argv) < 2:
    print('ERROR: no input parameters given\n')
    sys.exit(1)

print("encoding ffmpeg input: '{0}'".format(' '.join(sys.argv[1:])))

audio_tracks_env = os.environ.get('AUDIO_TRACKS')
if audio_tracks_env == None:
    print('ERROR: environment variable AUDIO_TRACKS is unset, please specify desired audio tracks as comma-separated list\n')
    sys.exit(1)
audio_tracks = [at.strip() for at in os.environ.get('AUDIO_TRACKS').split(',')]
for audio_track in audio_tracks:
    if audio_track not in config['audio_tracks']:
        print("ERROR: unknown audio-track '{0}' must be one of: {1}".format(audio_track, ', '.join(config['audio_tracks'].keys())))
        sys.exit(1)
    at = config['audio_tracks'][audio_track]
    print(' -> audio track: {0} ({1}, channels={2}), bitrate={3}k'.format(audio_track,
                                                                          at['input_stream'], ','.join(map(str, at['channels'])) if 'channels' in at else 'all', at['bitrate']))

quality_levels_env = os.environ.get('QUALITY_LEVELS')
if quality_levels_env == None:
    print('ERROR: environment variable QUALITY_LEVELS is unset, please specify desired quality-levels as comma-separated list\n')
    sys.exit(1)
quality_levels = [ql.strip() for ql in quality_levels_env.split(',')]
for quality_level in quality_levels:
    if quality_level not in config['quality_levels']:
        print("ERROR: unknown quality-level '{0}' must be one of: {1}".format(quality_level, ', '.join(config['quality_levels'].keys())))
        sys.exit(1)
    ql = config['quality_levels'][quality_level]
    print(' -> video quality-level: {0} ({1}), scale-to={2}, bitrate={3}k'.format(quality_level, ql['resolution'], ql['scale-to'], ql['bitrate']))

video_filter_common = os.environ.get('VIDEO_FILTER_COMMON')

if (config['hls']['segment_time'] % config['video_keyframe_distance']) != 0:
    print('ERROR: the configured HLS segment time ({0}s) is not a multiple of the keyframe distance ({1}s)'.format(
        config['hls']['segment_time'], config['video_keyframe_distance']))
    sys.exit(1)

print('')


# *******************************************************
# build ffmpeg command call

ffmpeg_args = ['ffmpeg', '-y']

# input
ffmpeg_args += sys.argv[1:]


# filter_complex
filter_complex_graphs = []

for audio_track in audio_tracks:
    at = config['audio_tracks'][audio_track]
    filters = []
    if 'channels' in at:
        filters.append('channelmap=map={0}'.format('|'.join(['{0}-{1}'.format(n, i) for i, n in enumerate(at['channels'])])))
    filter_complex_graphs.append('[{0}]{1}[a{2}]'.format(at['input_stream'], ','.join(filters) if filters else 'acopy', audio_track))

if len(quality_levels) == 1:
    filters = []
    if video_filter_common:
        filters = video_filter_common.split(',')
    ql = config['quality_levels'][quality_levels[0]]
    if ql['scale-to']:
        filters.append('scale=s={0[scale-to]}'.format(ql))
    filter_complex_graphs.append('[0:v]{0}[v{1}]'.format(','.join(filters) if filters else 'copy', quality_levels[0]))
else:
    outputs = []
    scalers = []

    filters = []
    if video_filter_common:
        filters = video_filter_common.split(',')
    for quality_level in quality_levels:
        ql = config['quality_levels'][quality_level]
        if ql['scale-to']:
            outputs.append('[vscaleto{0[resolution]}]'.format(ql))
            ql['name'] = quality_level
            scalers.append(ql)
        else:
            outputs.append('[v{0}]'.format(quality_level))

    filters.append('split={0}'.format(len(outputs)))
    filter_complex_graphs.append('[0:v]{0}{1}'.format(','.join(filters), ''.join(outputs)))

    for scaler in scalers:
        filter_complex_graphs.append('[vscaleto{0[resolution]}]scale=s={0[scale-to]}[v{0[name]}]'.format(scaler))

ffmpeg_args += ['-filter_complex', '; '.join(filter_complex_graphs)]


# mappings
for audio_track in audio_tracks:
    ffmpeg_args += ['-map', '[a{0}]'.format(audio_track)]

for quality_level in quality_levels:
    ffmpeg_args += ['-map', '[v{0}]'.format(quality_level)]


# encoder
ffmpeg_args += ['-c:a', 'aac']
for i, audio_track in enumerate(audio_tracks):
    ffmpeg_args += ['-b:a:{0}'.format(i), '{0}k'.format(config['audio_tracks'][audio_track]['bitrate'])]


ffmpeg_args += ['-force_key_frames:v', 'expr:gte(t,n_forced*{0})'.format(config['video_keyframe_distance']), '-flags:v', '+cgop']
ffmpeg_args += ['-c:v', 'libx264', '-crf:v', str(config['x264']['crf']), '-profile:v', config['x264']['profile'],
                '-preset:v', config['x264']['preset'], '-x264-params', ':'.join(config['x264']['params'])]
for i, quality_level in enumerate(quality_levels):
    bitrate = config['quality_levels'][quality_level]['bitrate']
    ffmpeg_args += ['-maxrate:v:{0}'.format(i), '{0}k'.format(bitrate), '-bufsize:v:{0}'.format(i), '{0}k'.format(bitrate*2)]


# muxer
hls_stream_variants = []
for i, audio_track in enumerate(audio_tracks):
    hls_stream_variants.append('a:{0},agroup:main,default:{1},language:{2},name:audio_{2}'.format(i, 'yes' if i == 0 else 'no', audio_track))

for i, quality_level in enumerate(quality_levels):
    hls_stream_variants.append('v:{0},name:video_{1},agroup:main'.format(i, quality_level))

hls_flags = '+omit_endlist+independent_segments+program_date_time'
if config['hls']['delete_segments']:
    hls_flags += '+delete_segments'

ffmpeg_args += ['-f', 'hls', '-var_stream_map', ' '.join(hls_stream_variants), '-master_pl_name', 'index.m3u8', '-master_pl_publish_rate', '10',
                '-hls_time', str(config['hls']['segment_time']), '-hls_list_size', str(config['hls']['list_size']),
                '-hls_allow_cache', '0', '-strftime', '1', '-hls_flags', hls_flags, '-ignore_io_errors', '1',
                '-hls_segment_filename', '{0}/{1}'.format(config['hls']['target'], '%v/%s.ts'), '{0}/{1}'.format(config['hls']['target'], '%v/index.m3u8')]


print('"%s"' % '" "'.join(ffmpeg_args))
print('')
# *******************************************************
# exec ffmpeg
# os.nice(-15)
# os.system('ionice -c 2 -n 0 -p %s' % os.getpid())
os.execv('/usr/bin/ffmpeg', ffmpeg_args)
