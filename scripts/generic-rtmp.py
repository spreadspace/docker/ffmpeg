#!/usr/bin/python3 -u

import sys
import os

import ruamel.yaml as yaml
import collections.abc


def config_update(d, u):
    if u == None:
        return d
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = config_update(d.get(k, {}), v)
        elif v is None:
            try:
                del d[k]
            except KeyError:
                pass
        else:
            d[k] = v
    return d


# *******************************************************
# configuration
config = {}

config['quality_levels'] = {}
config['quality_levels']['full'] = {'resolution': '1080p', 'bitrate': 5000, 'scale-to': None}
config['quality_levels']['high'] = {'resolution': '720p', 'bitrate': 3000, 'scale-to': 'hd720'}
config['quality_levels']['medium'] = {'resolution': '480p', 'bitrate': 2000, 'scale-to': 'hd480'}
config['quality_levels']['low'] = {'resolution': '360p', 'bitrate': 1000, 'scale-to': 'nhd'}
config['quality_levels']['mini'] = {'resolution': '240p', 'bitrate': 500, 'scale-to': 'fwqvga'}

config['audio_tracks'] = {}
config['audio_tracks']['all'] = {'input_stream': '0:a', 'bitrate': 512}
config['audio_tracks']['orig'] = {'input_stream': '0:a', 'channels': [0, 1], 'bitrate': 128}
config['audio_tracks']['interpreter'] = {'input_stream': '0:a', 'channels': [2], 'bitrate': 64}
config['audio_tracks']['en'] = {'input_stream': '0:a', 'channels': [4, 5], 'bitrate': 128}
config['audio_tracks']['de'] = {'input_stream': '0:a', 'channels': [6, 7], 'bitrate': 128}

config['x264'] = {}
config['x264']['crf'] = 21
config['x264']['profile'] = 'main'
config['x264']['preset'] = 'medium'
config['x264']['params'] = ['colorprim=bt709', 'transfer=bt709', 'colormatrix=bt709', 'fullrange=off']

config['video_keyframe_distance'] = 2

config['rtmp'] = {}
config['rtmp']['target'] = "rtmp://127.0.0.1/emc"


config_filename = os.environ.get('CONFIG_FILENAME')
if config_filename:
    with open(config_filename) as config_file:
        custom_config = yaml.load(config_file, Loader=yaml.SafeLoader)
        config_update(config, custom_config)
        print("successfully loaded config file: '%s'" % (config_filename))

print('configuration:\n')
print(yaml.dump(config, default_flow_style=False, default_style=''))
print('\n')


# *******************************************************
# check command-line arguments, environment variables and config

if len(sys.argv) < 2:
    print('ERROR: no input parameters given\n')
    sys.exit(1)

print("encoding ffmpeg input: '{0}'".format(' '.join(sys.argv[1:])))

audio_tracks_env = os.environ.get('AUDIO_TRACKS')
if audio_tracks_env == None:
    print('ERROR: environment variable AUDIO_TRACKS is unset, please specify desired audio tracks as comma-separated list\n')
    sys.exit(1)
audio_tracks = [at.strip() for at in os.environ.get('AUDIO_TRACKS').split(',')]
for audio_track in audio_tracks:
    if audio_track not in config['audio_tracks']:
        print("ERROR: unknown audio-track '{0}' must be one of: {1}".format(audio_track, ', '.join(config['audio_tracks'].keys())))
        sys.exit(1)
    at = config['audio_tracks'][audio_track]
    print(' -> audio track: {0} ({1}, channels={2}), bitrate={3}k'.format(audio_track,
                                                                          at['input_stream'], ','.join(map(str, at['channels'])) if 'channels' in at else 'all', at['bitrate']))

quality_levels_env = os.environ.get('QUALITY_LEVELS')
if quality_levels_env == None:
    print('ERROR: environment variable QUALITY_LEVELS is unset, please specify desired quality-levels as comma-separated list\n')
    sys.exit(1)
quality_levels = [ql.strip() for ql in quality_levels_env.split(',')]
for quality_level in quality_levels:
    if quality_level not in config['quality_levels']:
        print("ERROR: unknown quality-level '{0}' must be one of: {1}".format(quality_level, ', '.join(config['quality_levels'].keys())))
        sys.exit(1)
    ql = config['quality_levels'][quality_level]
    print(' -> video quality-level: {0} ({1}), scale-to={2}, bitrate={3}k'.format(quality_level, ql['resolution'], ql['scale-to'], ql['bitrate']))

video_filter_common = os.environ.get('VIDEO_FILTER_COMMON')

if len(audio_tracks) > 1:
    print('ERROR: rtmp/flv only supports one audio track!')
    sys.exit(1)

print('')


# *******************************************************
# build ffmpeg command call

ffmpeg_args = ['ffmpeg', '-y']

# input
ffmpeg_args += sys.argv[1:]


# filter_complex
filter_complex_graphs = []


at = config['audio_tracks'][audio_track]
filters = []
if 'channels' in at:
    filters.append('channelmap=map={0}'.format('|'.join(['{0}-{1}'.format(n, i) for i, n in enumerate(at['channels'])])))
if len(quality_levels) == 1:
    filter_complex_graphs.append('[{0}]{1}[a{2}]'.format(at['input_stream'], ','.join(filters) if filters else 'acopy', quality_levels[0]))
else:
    filters.append('asplit={0}'.format(len(quality_levels)))
    filter_complex_graphs.append('[{0}]{1}[a{2}]'.format(at['input_stream'], ','.join(filters) if filters else 'acopy', '][a'.join(quality_levels)))

if len(quality_levels) == 1:
    filters = []
    if video_filter_common:
        filters = video_filter_common.split(',')
    ql = config['quality_levels'][quality_levels[0]]
    if ql['scale-to']:
        filters.append('scale=s={0[scale-to]}'.format(ql))
    filter_complex_graphs.append('[0:v]{0}[v{1}]'.format(','.join(filters) if filters else 'copy', quality_levels[0]))
else:
    outputs = []
    scalers = []

    filters = []
    if video_filter_common:
        filters = video_filter_common.split(',')
    for quality_level in quality_levels:
        ql = config['quality_levels'][quality_level]
        if ql['scale-to']:
            outputs.append('[vscaleto{0[resolution]}]'.format(ql))
            ql['name'] = quality_level
            scalers.append(ql)
        else:
            outputs.append('[v{0}]'.format(quality_level))

    filters.append('split={0}'.format(len(outputs)))
    filter_complex_graphs.append('[0:v]{0}{1}'.format(','.join(filters), ''.join(outputs)))

    for scaler in scalers:
        filter_complex_graphs.append('[vscaleto{0[resolution]}]scale=s={0[scale-to]}[v{0[name]}]'.format(scaler))

ffmpeg_args += ['-filter_complex', '; '.join(filter_complex_graphs)]


# mappings/encoder/muxer
for quality_level in quality_levels:
    ffmpeg_args += ['-map', '[a{0}]'.format(quality_level)]
    ffmpeg_args += ['-map', '[v{0}]'.format(quality_level)]

    ffmpeg_args += ['-c:a', 'aac', '-b:a', '{0}k'.format(config['audio_tracks'][audio_tracks[0]]['bitrate'])]

    ffmpeg_args += ['-force_key_frames:v', 'expr:gte(t,n_forced*{0})'.format(config['video_keyframe_distance']), '-flags:v', '+cgop']
    ffmpeg_args += ['-c:v', 'libx264', '-crf:v', str(config['x264']['crf']), '-profile:v', config['x264']['profile'],
                    '-preset:v', config['x264']['preset'], '-x264-params', ':'.join(config['x264']['params'])]
    bitrate = config['quality_levels'][quality_level]['bitrate']
    ffmpeg_args += ['-maxrate:v', '{0}k'.format(bitrate), '-bufsize:v', '{0}k'.format(bitrate*2)]

    # muxer
    ffmpeg_args += ['-f', 'flv', '{0}/{1}'.format(config['rtmp']['target'], quality_level)]


print('"%s"' % '" "'.join(ffmpeg_args))
print('')
# *******************************************************
# exec ffmpeg
# os.nice(-15)
# os.system('ionice -c 2 -n 0 -p %s' % os.getpid())
os.execv('/usr/bin/ffmpeg', ffmpeg_args)
