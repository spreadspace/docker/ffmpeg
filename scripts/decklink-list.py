#!/usr/bin/python3 -u
import sys
import subprocess
import re
import signal


# *******************************************************
# check command-line arguments

devices = {}

ffmpeg_sources = subprocess.run(['/usr/bin/ffmpeg', '-hide_banner', '-sources', 'decklink'], capture_output=True)
re_dev = re.compile('^\s*[0-9A-Fa-f:]+\s+\[(.*)\]')
for line in ffmpeg_sources.stdout.decode('utf8').splitlines():
    m = re_dev.match(line)
    if m:
        devices[m.group(1)] = []

print("Decklink Devices:")
if devices:
    re_fmt = re.compile('^\s+([0-9A-Za-z]+)\s+(.*)$')
    for d in devices.keys():
        print(" * {0}".format(d))
        ffmpeg_formats = subprocess.run(['/usr/bin/ffmpeg', '-hide_banner', '-f', 'decklink', '-list_formats', '1', '-i', d], capture_output=True)
        for line in ffmpeg_formats.stderr.decode('utf8').splitlines():
            m = re_fmt.match(line)
            if m and m.group(1) != 'format_code':
                print("   - %s\t%s" % (m.group(1), m.group(2)))
else:
    print(" * no decklink devices found!")

signal.pause()
