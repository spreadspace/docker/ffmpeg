#!/usr/bin/python3 -u

import sys
import os

import ruamel.yaml as yaml
import collections.abc


def config_update(d, u):
    if u == None:
        return d
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = config_update(d.get(k, {}), v)
        elif v is None:
            try:
                del d[k]
            except KeyError:
                pass
        else:
            d[k] = v
    return d


# *******************************************************
# configuration
config = {}

config['quality_levels'] = {}
config['quality_levels']['full'] = {'resolution': '1080p', 'bitrate': 5000, 'scale-to': None}
config['quality_levels']['high'] = {'resolution': '720p', 'bitrate': 3000, 'scale-to': 'hd720'}
config['quality_levels']['medium'] = {'resolution': '480p', 'bitrate': 2000, 'scale-to': 'hd480'}
config['quality_levels']['low'] = {'resolution': '360p', 'bitrate': 1000, 'scale-to': 'nhd'}
config['quality_levels']['mini'] = {'resolution': '240p', 'bitrate': 500, 'scale-to': 'fwqvga'}

config['audio_tracks'] = {}
config['audio_tracks']['all'] = {'input_stream': '0:a', 'bitrate': 512}
config['audio_tracks']['orig'] = {'input_stream': '0:a', 'channels': [0, 1], 'bitrate': 128}
config['audio_tracks']['interpreter'] = {'input_stream': '0:a', 'channels': [2], 'bitrate': 64}
config['audio_tracks']['en'] = {'input_stream': '0:a', 'channels': [4, 5], 'bitrate': 128}
config['audio_tracks']['de'] = {'input_stream': '0:a', 'channels': [6, 7], 'bitrate': 128}

config['hls'] = {}
config['hls']['segment_time'] = 10
config['hls']['list_size'] = 3
config['hls']['delete_segments'] = True
config['hls']['target'] = '.'


config_filename = os.environ.get('CONFIG_FILENAME')
if config_filename:
    with open(config_filename) as config_file:
        custom_config = yaml.load(config_file, Loader=yaml.SafeLoader)
        config_update(config, custom_config)
        print("successfully loaded config file: '%s'" % (config_filename))

print('configuration:\n')
print(yaml.dump(config, default_flow_style=False, default_style=''))
print('\n')


# *******************************************************
# check command-line arguments, environment variables and config

if len(sys.argv) < 2:
    print('ERROR: no input parameters given\n')
    sys.exit(1)

print("encoding ffmpeg input: '{0}'".format(' '.join(sys.argv[1:])))

audio_tracks_env = os.environ.get('AUDIO_TRACKS')
if audio_tracks_env == None:
    print('ERROR: environment variable AUDIO_TRACKS is unset, please specify desired audio tracks as comma-separated list\n')
    sys.exit(1)
audio_tracks = [at.strip() for at in os.environ.get('AUDIO_TRACKS').split(',')]
for audio_track in audio_tracks:
    if audio_track not in config['audio_tracks']:
        print("ERROR: unknown audio-track '{0}' must be one of: {1}".format(audio_track, ', '.join(config['audio_tracks'].keys())))
        sys.exit(1)
    at = config['audio_tracks'][audio_track]
    print(' -> audio track: {0} ({1}, channels={2}), bitrate={3}k'.format(audio_track,
                                                                          at['input_stream'], ','.join(map(str, at['channels'])) if 'channels' in at else 'all', at['bitrate']))

quality_levels_env = os.environ.get('QUALITY_LEVELS')
if quality_levels_env == None:
    print('ERROR: environment variable QUALITY_LEVELS is unset, please specify desired quality-levels as comma-separated list\n')
    sys.exit(1)
quality_levels = [ql.strip() for ql in quality_levels_env.split(',')]
for quality_level in quality_levels:
    if quality_level not in config['quality_levels']:
        print("ERROR: unknown quality-level '{0}' must be one of: {1}".format(quality_level, ', '.join(config['quality_levels'].keys())))
        sys.exit(1)
    ql = config['quality_levels'][quality_level]
    print(' -> video quality-level: {0} ({1}), scale-to={2}, bitrate={3}k'.format(quality_level, ql['resolution'], ql['scale-to'], ql['bitrate']))

print('')


# *******************************************************
# build ffmpeg command call

ffmpeg_args = ['ffmpeg', '-y']

# input
ffmpeg_args += sys.argv[1:]


# mappings
for i, audio_track in enumerate(audio_tracks):
    ffmpeg_args += ['-map', '0:a:{0}'.format(i)]

for i, quality_level in enumerate(quality_levels):
    ffmpeg_args += ['-map', '0:v:{0}'.format(i)]


# encoder
ffmpeg_args += ['-c:a', 'copy']
for i, audio_track in enumerate(audio_tracks):
    # HLS muxer needs to know the bitrates to generate the master playlist
    ffmpeg_args += ['-b:a:{0}'.format(i), '{0}k'.format(config['audio_tracks'][audio_track]['bitrate'])]


ffmpeg_args += ['-c:v', 'copy']
for i, quality_level in enumerate(quality_levels):
    # HLS muxer needs to know the bitrates to generate the master playlist
    ffmpeg_args += ['-b:v:{0}'.format(i), '{0}k'.format(config['quality_levels'][quality_level]['bitrate'])]


# muxer
hls_stream_variants = []
for i, audio_track in enumerate(audio_tracks):
    hls_stream_variants.append('a:{0},agroup:main,default:{1},language:{2},name:audio_{2}'.format(i, 'yes' if i == 0 else 'no', audio_track))

for i, quality_level in enumerate(quality_levels):
    hls_stream_variants.append('v:{0},name:video_{1},agroup:main'.format(i, quality_level))

hls_flags = '+omit_endlist+independent_segments+program_date_time'
if config['hls']['delete_segments']:
    hls_flags += '+delete_segments'

ffmpeg_args += ['-f', 'hls', '-var_stream_map', ' '.join(hls_stream_variants), '-master_pl_name', 'index.m3u8', '-master_pl_publish_rate', '10',
                '-hls_time', str(config['hls']['segment_time']), '-hls_list_size', str(config['hls']['list_size']),
                '-hls_allow_cache', '0', '-strftime', '1', '-hls_flags', hls_flags, '-ignore_io_errors', '1',
                '-hls_segment_filename', '{0}/{1}'.format(config['hls']['target'], '%v/%s.ts'), '{0}/{1}'.format(config['hls']['target'], '%v/index.m3u8')]


print('"%s"' % '" "'.join(ffmpeg_args))
print('')
# *******************************************************
# exec ffmpeg
# os.nice(-15)
# os.system('ionice -c 2 -n 0 -p %s' % os.getpid())
os.execv('/usr/bin/ffmpeg', ffmpeg_args)
