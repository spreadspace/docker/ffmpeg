#!/usr/bin/python3 -u

import sys
import os

import ruamel.yaml as yaml
import collections.abc


def config_update(d, u):
    if u == None:
        return d
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = config_update(d.get(k, {}), v)
        elif v is None:
            try:
                del d[k]
            except KeyError:
                pass
        else:
            d[k] = v
    return d


# *******************************************************
# configuration
config = {}

config['quality_levels'] = {}
config['quality_levels']['full'] = {'resolution': '1080p', 'bitrate': 50000, 'scale-to': None}
config['quality_levels']['high'] = {'resolution': '720p', 'bitrate': 30000, 'scale-to': 'hd720'}
config['quality_levels']['medium'] = {'resolution': '480p', 'bitrate': 20000, 'scale-to': 'hd480'}
config['quality_levels']['low'] = {'resolution': '360p', 'bitrate': 10000, 'scale-to': 'nhd'}
config['quality_levels']['mini'] = {'resolution': '240p', 'bitrate': 5000, 'scale-to': 'fwqvga'}

config['audio_tracks'] = {}
config['audio_tracks']['all'] = {'input_stream': '0:a', 'bitrate': 640}
config['audio_tracks']['orig'] = {'input_stream': '0:a', 'channels': [0, 1], 'bitrate': 160}
config['audio_tracks']['interpreter'] = {'input_stream': '0:a', 'channels': [2], 'bitrate': 80}
config['audio_tracks']['en'] = {'input_stream': '0:a', 'channels': [4, 5], 'bitrate': 160}
config['audio_tracks']['de'] = {'input_stream': '0:a', 'channels': [6, 7], 'bitrate': 160}

config['x264'] = {}
config['x264']['crf'] = 16
config['x264']['profile'] = 'high422'
config['x264']['preset'] = 'medium'
config['x264']['params'] = ['colorprim=bt709', 'transfer=bt709', 'colormatrix=bt709', 'fullrange=off']

config['record'] = {}
config['record']['segment_time'] = 3600
config['record']['clocktime_offset'] = 0
config['record']['target'] = '.'


config_filename = os.environ.get('CONFIG_FILENAME')
if config_filename:
    with open(config_filename) as config_file:
        custom_config = yaml.load(config_file, Loader=yaml.SafeLoader)
        config_update(config, custom_config)
        print("successfully loaded config file: '%s'" % (config_filename))

print('configuration:\n')
print(yaml.dump(config, default_flow_style=False, default_style=''))
print('\n')


# *******************************************************
# check command-line arguments, environment variables and config

if len(sys.argv) < 2:
    print('ERROR: no input parameters given\n')
    sys.exit(1)

print("encoding ffmpeg input: '{0}'".format(' '.join(sys.argv[1:])))

audio_tracks_env = os.environ.get('AUDIO_TRACKS')
if audio_tracks_env == None:
    print('ERROR: environment variable AUDIO_TRACKS is unset, please specify desired audio tracks as comma-separated list\n')
    sys.exit(1)
audio_tracks = [at.strip() for at in os.environ.get('AUDIO_TRACKS').split(',')]
for audio_track in audio_tracks:
    if audio_track not in config['audio_tracks']:
        print("ERROR: unknown audio-track '{0}' must be one of: {1}".format(audio_track, ', '.join(config['audio_tracks'].keys())))
        sys.exit(1)
    at = config['audio_tracks'][audio_track]
    print(' -> audio track: {0} ({1}, channels={2}), bitrate={3}k'.format(audio_track,
                                                                          at['input_stream'], ','.join(map(str, at['channels'])) if 'channels' in at else 'all', at['bitrate']))

quality_levels_env = os.environ.get('QUALITY_LEVELS')
if quality_levels_env == None:
    print('ERROR: environment variable QUALITY_LEVELS is unset, please specify desired quality-levels as comma-separated list\n')
    sys.exit(1)
quality_levels = [ql.strip() for ql in quality_levels_env.split(',')]
for quality_level in quality_levels:
    if quality_level not in config['quality_levels']:
        print("ERROR: unknown quality-level '{0}' must be one of: {1}".format(quality_level, ', '.join(config['quality_levels'].keys())))
        sys.exit(1)
    ql = config['quality_levels'][quality_level]
    print(' -> video quality-level: {0} ({1}), scale-to={2}, bitrate={3}k'.format(quality_level, ql['resolution'], ql['scale-to'], ql['bitrate']))

video_filter_common = os.environ.get('VIDEO_FILTER_COMMON')

if len(quality_levels) > 1:
    print('ERROR: recorder only supports a single quality level!')
    sys.exit(1)

print('')


# *******************************************************
# build ffmpeg command call

ffmpeg_args = ['ffmpeg', '-y']

# input
ffmpeg_args += sys.argv[1:]


# filter_complex
filter_complex_graphs = []

for audio_track in audio_tracks:
    at = config['audio_tracks'][audio_track]
    filters = []
    if 'channels' in at:
        filters.append('channelmap=map={0}'.format('|'.join(['{0}-{1}'.format(n, i) for i, n in enumerate(at['channels'])])))
    filter_complex_graphs.append('[{0}]{1}[a{2}]'.format(at['input_stream'], ','.join(filters) if filters else 'acopy', audio_track))

filters = []
if video_filter_common:
    filters = video_filter_common.split(',')
ql = config['quality_levels'][quality_levels[0]]
if ql['scale-to']:
    filters.append('scale=s={0[scale-to]}'.format(ql))
filter_complex_graphs.append('[0:v]{0}[v{1}]'.format(','.join(filters) if filters else 'copy', quality_levels[0]))

ffmpeg_args += ['-filter_complex', '; '.join(filter_complex_graphs)]


# mappings
for audio_track in audio_tracks:
    ffmpeg_args += ['-map', '[a{0}]'.format(audio_track)]
ffmpeg_args += ['-map', '[v{0}]'.format(quality_levels[0])]


# encoder
ffmpeg_args += ['-c:a', 'aac']
for i, audio_track in enumerate(audio_tracks):
    ffmpeg_args += ['-b:a:{0}'.format(i), '{0}k'.format(config['audio_tracks'][audio_track]['bitrate'])]
    ffmpeg_args += ['-metadata:s:a:{0}'.format(i), 'language={0}'.format(audio_track)]


ffmpeg_args += ['-g:v', '1', '-c:v', 'libx264', '-crf:v', str(config['x264']['crf']), '-profile:v', config['x264']['profile'],
                '-preset:v', config['x264']['preset'], '-x264-params', ':'.join(config['x264']['params'])]
bitrate = config['quality_levels'][quality_levels[0]]['bitrate']
ffmpeg_args += ['-maxrate:v', '{0}k'.format(bitrate), '-bufsize:v', '{0}k'.format(bitrate*2)]


# muxer
ffmpeg_args += ['-f', 'segment', '-segment_time', str(config['record']['segment_time']), '-segment_atclocktime', '1', '-segment_clocktime_offset', str(config['record']['clocktime_offset']),
                '-strftime', '1', '-reset_timestamps', '1', '{0}/{1}'.format(config['record']['target'], '%Y-%m-%d_%H-%M-%S.mkv')]


print('"%s"' % '" "'.join(ffmpeg_args))
print('')
# *******************************************************
# exec ffmpeg
# os.nice(-15)
# os.system('ionice -c 2 -n 0 -p %s' % os.getpid())
os.execv('/usr/bin/ffmpeg', ffmpeg_args)
